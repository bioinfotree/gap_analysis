# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# maps a portion of flanking contigs to the reference to infer real gap distribution

GAPS_EXTENDED_FASTA = $(addsuffix .flanking.contigs.bed,$(SAMPLES))
%.flanking.contigs.bed:
	ln -sf ../phase_1/$@ $@

SCAFFOLDS = $(addsuffix .scaffolds.fasta,$(SAMPLES))
%.scaffolds.fasta:
	ln -sf ../phase_1/$@ $@

GAPS_EXTENDED_BED = $(addsuffix .gaps.extended.bed,$(SAMPLES))
%.gaps.extended.bed: %.flanking.contigs.bed
	select_columns 4 1 2 3 5 6 7 8 9 10 11 <$< \
	| bawk '!/^[$$,\#+]/ { \
	if ( $$11 > 0 ) \
		{ \
			LEFT=$$7; RIGHT=$$8; \
			if ( $$10 > $(EXTENSION) ) LEFT=$$8-$(EXTENSION); \   * if leftside contig is longer than extension, it is truncated to extension *
			print $$2, LEFT, RIGHT, $$1".left", $$3, $$4, $$5; \
		} \
	else if ( $$11 < 0 ) \
		{ \
			LEFT=$$7; RIGHT=$$8; \
			if ( $$10 > $(EXTENSION) ) RIGHT=$$7+$(EXTENSION); \   * as above but inverted *
			print $$2, LEFT, RIGHT, $$1".right", $$3, $$4, $$5; \
			} \
	}' \
	| bsort -k1,1 -k4,4 >$@

GAPS_EXTENDED_FASTA = $(addsuffix .gaps.extended.fasta,$(SAMPLES))
%.gaps.extended.fasta: %.scaffolds.fasta %.gaps.extended.bed
	$(call load_modules); \
	bedtools getfasta -fi $< -bed $^2 -fo stdout -name >$@


GAPS_MAP = $(addsuffix .map,$(SAMPLES))

.META: %.map
	1	qseqid
	2	qlen
	3	sseqid
	4	slen
	5	pident
	6	length
	7	mismatch
	8	gapopen
	9	qstart
	10	qend
	11	sstart
	12	send
	13	evalue
	14	bitscore

.PRECIOUS: %.map

%.map: $(REF) %.gaps.extended.fasta
	!threads
	$(call load_modules); \
	blastn \
	-db $< \
	-query $^2 \
	-evalue 1e-06 \
	-num_threads $$THREADNUM \
	-outfmt '6 qseqid qlen sseqid slen pident length mismatch gapopen qstart qend sstart send evalue bitscore' \
	-max_target_seqs 1 \
	| sort -u -k1,1 --merge >$@   * select the best alignment *


REFERENCE_GAPS = $(addsuffix .reference.gaps.bed,$(SAMPLES))
%.reference.gaps.bed: %.map
	select_columns 1 3 11 12 <$< \
	| sed 's/\./\t/' \
	| expand -t 1 \
	| sed 's/\s/\t/' \
	| set_collapse 2 -g " " \
	| sed 's/\s/\t/g' \
	| bawk '!/^[\#+,$$]/ { \
	if ( $$3 == $$7 && $$2 == "left" && $$6 == "right" ) { \
		LEFT_START=$$4; LEFT_END=$$5; \
		if ( LEFT_START > LEFT_END ) { LEFT_START=$$5; LEFT_END=$$4; } \   * change order of coordinates *
		RIGHT_START=$$8; RIGHT_END=$$9; \
		if ( RIGHT_START > RIGHT_END ) { RIGHT_START=$$9; RIGHT_END=$$8; } \   * change order of coordinates *
		#print $$1, LEFT_START, LEFT_END, RIGHT_START, RIGHT_END, $$3; \
		if ( LEFT_START < RIGHT_START ) { print $$3, LEFT_END, RIGHT_START, $$1, RIGHT_START-LEFT_END }; \
		if ( LEFT_START > RIGHT_START ) { print $$3, RIGHT_END, LEFT_START, $$1, LEFT_START-RIGHT_END }; \
	} \
	else \
	{ if ( $$2 != "left" || $$6 != "right" ) print $$0>"$*.single.ends"; else print $$0 >"$*.map.different.chr"; } \
	}' >$@

MAP_DIFFERENT_CHR = $(addsuffix .map.different.chr,$(SAMPLES))
%.map.different.chr: %.reference.gaps.bed
	touch $@

SINGLE_ENDS = $(addsuffix .single.ends,$(SAMPLES))
%.single.ends: %.reference.gaps.bed
	touch $@

REFERENCE_GAPS_PDF = $(addsuffix .reference.gaps.pdf,$(SAMPLES))
%.reference.gaps.pdf: %.reference.gaps.bed
	this_distrib_plot -t histogram -b 500 --xlim="-2000:13500" --ylim="0:13000" --xlab="gap length" --ylab="number of gaps" --title "$* gap lengths distribution" -o $@ 5 <$<



REFERENCE_GAPS_FASTA = $(addsuffix .reference.gaps.fasta,$(SAMPLES))
%.reference.gaps.fasta: $(REF) %.reference.gaps.bed
	$(call load_modules); \
	bedtools getfasta -fi $< -bed <( bawk '!/^[\#+,$$]/ { if ( $$5 > 0 ) print $$0; }' <$^2) \   * remove negative gaps *
	-fo stdout -name >$@



.META: %.stats stats.all
	1	sample
	2	total gaps with 2500 bp flanks (#)
	3	gaps mapped on reference (#)
	4	gap flanks that map on different chrs (#)
	5	at leat one map on Random or UnKnown
	6	single gap flank (found at extremities. #)
	7	median (Non Negative Gaps - NNG, bp)
	8	mean standard deviation (NNG, bp)
	9	min (NNG, bp)
	10	max (NNG, bp)
	11	total lenght (NNG, bp)


STATS = $(addsuffix .stats,$(SAMPLES))
%.stats: %.reference.gaps.bed %.map.different.chr %.single.ends
	wc -l $^ \
	| sed 's/^ *//' \
	| tr [:blank:] \\t \
	| select_columns 2 1 \
	| transpose \
	| sed 's/^/$*\t/' \
	| unhead \
	| paste - \
	<(bawk 'BEGIN { unknown=0; } !/^[\#+,$$]/ { if ( $$3 ~ /chrUn/ || $$7 ~ /chrUn/ || $$3 ~ /random/ || $$7 ~ /random/) unknown++; } END { print unknown; }' $^2) \
	<(bawk '!/^[\#+,$$]/ { if ( $$5 > 0 ) print $$0; }' <$< \   * remove negative gaps *
	| stat_base --precision=10 --median --mean-stdev --min --max --total 5) \
	| select_columns 1 5 2 3 6 4 7 8 9 10 11 >$@


stats.all: $(STATS)
	cat $^ >$@

reference.gaps.distribution.all.pdf: $(REFERENCE_GAPS_PDF)
	$(call load_modules); \
	gs -dBATCH -dNOPAUSE -dBATCH -dSAFER -dAutoRotatePages=/None -q -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite -sOutputFile=$@ $^   * merge pdf vertically without rotation *



.PHONY: test
test:
	@echo 

ALL += $(GAPS_EXTENDED_FASTA) \
	$(GAPS_MAP) \
	$(REFERENCE_GAPS) \
	$(MAP_DIFFERENT_CHR) \
	$(SINGLE_ENDS) \
	$(REFERENCE_GAPS_FASTA) \
	$(REFERENCE_GAPS_PDF) \
	stats.all \
	reference.gaps.distribution.all.pdf

CLEAN += 

INTERMEDIATE += 