# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

EXTENSION ?= 2500

context prj/check_hemizygosity


# this function install all the links at once
SCAFFOLDS = $(addsuffix .scaffolds.fasta,$(SAMPLES))
CONTIGS = $(addsuffix .contigs.fasta,$(SAMPLES))
FINAL_SUMMARIES = $(addsuffix .final.summary,$(SAMPLES))
%.scaffolds.fasta %.contigs.fasta %.final.summary:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf $(call get,$(SAMPLE),SCAFFOLD) $(SAMPLE).scaffolds.fasta) \
		$(shell ln -sf $(call get,$(SAMPLE),CONTIG) $(SAMPLE).contigs.fasta) \
		$(shell ln -sf $(call get,$(SAMPLE),FINAL_SUMMARY) $(SAMPLE).final.summary) \
	) \
	&& sleep 3

GAPS_BED = $(addsuffix .gaps.bed,$(SAMPLES))
%.gaps.bed: %.final.summary
	scaffolds2contigs <$< \
	| cut -f 1-5 \
	| grep -E 'gap_[0-9]+' >$@

GAPS_STATS = $(addsuffix .gaps.stats,$(SAMPLES))
%.gaps.stats: %.gaps.bed
	stat_base --precision=10 --median --mean-stdev --min --max --total 5 <$< >$@

CONTIGS_BED = $(addsuffix .contigs.bed,$(SAMPLES))
%.contigs.bed: %.final.summary
	scaffolds2contigs <$< \
	| cut -f 1-5 \
	| grep -E 'contig_[0-9]+' >$@

CONTIGS_NOT_IN_FASTA_BED = $(addsuffix .contigs.in.fasta.bed,$(SAMPLES))
%.contigs.not.in.fasta.bed: %.contigs.bed %.contigs.fasta
	filter_1col -v 4 <(fasta_length <$^2 | cut -f1 | bsort) <$< >$@

CONTIGS_IN_FASTA_BED = $(addsuffix .contigs.in.fasta.bed,$(SAMPLES))
%.contigs.in.fasta.bed: %.contigs.bed %.contigs.fasta
	filter_1col 4 <(fasta_length <$^2 | cut -f1 | bsort) <$< >$@

GAPS_LEN_DISTRIBUTION_PDF = $(addsuffix .gaps.len.distribution.pdf,$(SAMPLES))
%.gaps.len.distribution.pdf: %.gaps.bed
	this_distrib_plot -t histogram -b 500 --xlim="0:13500" --ylim="0:50000" --xlab="gap length" --ylab="number of gaps" --title "$* gap lengths distribution" -o $@ 5 <$<

NON-NEGATIVE_GAPS_BED = $(addsuffix .non-negative.gaps.bed,$(SAMPLES))
%.non-negative.gaps.bed: %.gaps.bed
	bawk '!/^[$$,\#+]/ { if ( $$5 > 1 ) print $$0; }' <$< >$@

NON-NEGATIVE_GAPS_DISTRIBUTION_PDF = $(addsuffix .non-negative.gaps.distribution.pdf,$(SAMPLES))
%.non-negative.gaps.distribution.pdf: %.non-negative.gaps.bed
	this_distrib_plot -t histogram -b 500 --xlim="0:13500" --ylim="0:15500" --xlab="gap length" --ylab="number of gaps" --title "$* non-negative gap lengths distribution" -o $@ 5 <$<


FLANKING_CONTIGS_BED = $(addsuffix .flanking.contigs.bed,$(SAMPLES))
# select contigs before and after every given gap
%.flanking.contigs.bed: %.gaps.bed %.contigs.in.fasta.bed
	$(call load_modules); \
	bedtools closest -io -D a -b <(bsort -k1,1 -k2,2n <$<) -a <(bsort -k1,1 -k2,2n <$^2) \   * -a and -b shoud be exchange but this leads to loss of some interactions between gaps and flanking contigs *
	| select_columns 6 7 8 9 10 1 2 3 4 5 11 \
	| bsort -k1,1 -k2,2n -k7,7n \
	| bawk '!/^[$$,\#+]/ { if ( $$5 > 1 ) print $$0; }' >$@   * again remove negative gaps *

# collapse every gap and its flanking contigs
%.flanking.contigs.bed.tmp: %.flanking.contigs.bed
	select_columns 4 1 2 3 5 6 7 8 9 10 11 <$< \
	| bsort -k1,1 \
	| expand -t 1 \
	| sed -E 's/ /\t/' \
	| collapsesets -g " " -o 2 \
	| tr [:blank:] \\t >$@

GAPS_EXTENDED_BED = $(addsuffix .gaps.extended.bed,$(SAMPLES))
# extend gaps of a given range no more that its flanking contigs
%.gaps.extended.bed: %.flanking.contigs.bed.tmp
	bawk '!/^[$$,\#+]/ { \
	if ( $$11 > 0 && $$21 < 0 && $$2 == $$6 && $$6 == $$12 && $$12 == $$16 ) \
		{ \
			LEFT=$$7; RIGHT=$$18; \
			if ( $$10 > $(EXTENSION) ) LEFT=$$3-$(EXTENSION); \   * if leftside contig is longer than extension, it is truncated to extension *
			if ( $$20 > $(EXTENSION) ) RIGHT=$$4+$(EXTENSION); \   * if rightside contig is longer than extension, it is truncated to extension *
			print $$2, LEFT, RIGHT, $$1, $$2, $$3, $$4, $$5; \
		} \
	else if ( $$11 < 0 && $$21 > 0 && $$2 == $$6 && $$6 == $$12 && $$12 == $$16 ) \
		{ \
			LEFT=$$17; RIGHT=$$8; \
			if ( $$10 > $(EXTENSION) ) RIGHT=$$4+$(EXTENSION); \   * as above but inverted *
			if ( $$20 > $(EXTENSION) ) LEFT=$$3-$(EXTENSION); \
			print $$2, LEFT, RIGHT, $$1, $$2, $$3, $$4, $$5; \
			} \
	}' <$< \
	| bsort -k1,1 >$@

# gaps.extended.bed: non-negative.gaps.bed scaffolds.fasta
	# $(call load_modules); \
	# bedtools slop -i $< -g <(fasta_length <$^2) -b $(EXTENSION) \
	# | bawk '!/^[$$,\#+]/ { if ( ( $$3 - $$2 - $$5 )/2 >= $(EXTENSION) ) print $$0; }' >$@   * retains only gaps with desidered extension at both ends *

GAPS_EXTENDED_FASTA = $(addsuffix .gaps.extended.fasta,$(SAMPLES))
# extract regions from fasta
%.gaps.extended.fasta: %.scaffolds.fasta %.gaps.extended.bed
	$(call load_modules); \
	bedtools getfasta -fi $< -bed $^2 -fo stdout -name >$@

.META: stats.all
	1	assembly
	2	gaps in scaffolds
	3	contigs in scaffolds
	4	contigs fasta file
	5	contigs not in fasta file
	6	non-negative gaps (NNG)
	7	gaps extend no more then $(EXTENSION) bp at both ends
	8	total
	9	median (NNG)
	10	mean standard deviation (NNG)
	11	min (NNG)
	12	max (NNG)
	13	total lenght (NNG)
	14	total lenght of all gaps


STATS = $(addsuffix .stats,$(SAMPLES))
%.stats: %.gaps.bed %.contigs.bed %.contigs.in.fasta.bed %.contigs.not.in.fasta.bed %.non-negative.gaps.bed %.gaps.extended.bed
	wc -l $^ \
	| sed 's/^ *//' \
	| tr [:blank:] \\t \
	| select_columns 2 1 \
	| transpose \
	| sed 's/^/$*\t/' \
	| unhead \
	| paste - \
	<(stat_base --precision=10 --median --mean-stdev --min --max --total 5 <$^5) \
	<(stat_base --precision=10 --total 5 <$<) >$@   * non-negative gaps bed *

stats.all: $(STATS)
	cat $^ >$@

non-negative.gaps.distribution.all.pdf: $(NON-NEGATIVE_GAPS_DISTRIBUTION_PDF)
	$(call load_modules); \
	gs -dBATCH -dNOPAUSE -dBATCH -dSAFER -dAutoRotatePages=/None -q -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite -sOutputFile=$@ $^   * merge pdf vertically without rotation *

#	montage -density 300x300 -quality 100 -mode concatenate -tile 2x $^ $@




.PHONY: test
test:
	@echo 

ALL += $(CONTIGS_NOT_IN_FASTA_BED) \
	$(CONTIGS_IN_FASTA_BED) \
	$(GAPS_STATS) \
	$(GAPS_LEN_DISTRIBUTION_PDF) \
	$(NON-NEGATIVE_GAPS_DISTRIBUTION_PDF) \
	$(FLANKING_CONTIGS_BED) \
	$(GAPS_EXTENDED_BED) \
	$(GAPS_EXTENDED_FASTA) \
	stats.all \
	non-negative.gaps.distribution.all.pdf

CLEAN += $(SCAFFOLDS) $(CONTIGS) $(FINAL_SUMMARIES) $(wildcard *.fai)

INTERMEDIATE += $(wildcard *.tmp) $(STATS)